FROM debian:11-slim
RUN apt update &&\
    apt install -y dumb-init wget
RUN wget https://download.cuberite.org/linux-x86_64/Cuberite.tar.gz &&\
    mkdir /cuberite &&\
    tar -xvf Cuberite.tar.gz -C /cuberite &&\
    rm -f Cuberite.tar.gz
ADD run /usr/local/bin/run
RUN chmod +x /usr/local/bin/run
RUN apt purge -y wget &&\
    apt autoremove --purge -y &&\
    apt-get clean
WORKDIR /cuberite
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["run"]
VOLUME /data
EXPOSE 8080 25565
